<?php
require_once('config.php');
require_once('functions.php');
?>
<html>
<head>
    <title>Grouper</title>
    <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    <script src="grouper.js"></script>
    <link rel="stylesheet" type="text/css" href="grouper.css">
</head>
<body>
    <h1>Grouper</h1>
    <div class="loader">Verwerken..</div>

    <?php
    $groups = $dbh->query('SELECT * FROM groups')->fetchAll();
    $items = $dbh->query('SELECT * FROM items')->fetchAll();

    foreach ($groups as $group) {
        if ($group['parent_group_id'] == 0) {
            printNestedGroupsAndItems($group, $groups, $items);
        }
    }

    foreach ($items as $item) {
        if ($item['parent_group_id'] == 0) {
            printItem($item);
        }
    }
    ?>

    <div class="add-forms">
        <form>
            <label class="add add-group">
                <span>Group:</span>
                <input type="text" name="group-name" id="group-name" placeholder="Group name" />
                in
                <select class="group-select" name="parent-group-id">
                    <option value="0">root</option>
                    <?php
                    foreach ($groups as $group) {
                        echo "<option value='{$group['id']}'>{$group['name']}({$group['id']})</option>";
                    }
                    ?>
                </select>
                <input type="submit" value="Add group" />
            </label>
        </form>
        <form>
            <label class="add add-item">
                <span>Item:</span>
                <input type="text" name="item-name" placeholder="Item name" />
                in
                <select class="group-select" name="parent-group-id">
                    <option value="0">root</option>
                    <?php
                    foreach ($groups as $group) {
                        echo "<option value='{$group['id']}'>{$group['name']}({$group['id']})</option>";
                    }
                    ?>
                </select>
                <input type="submit" value="Add item">
            </label>
        </form>
    </div>

</body>
</html>

