<?php
function printItem($item)
{
    echo '<div class="item">';
    echo "<a href='/item.php?name={$item['name']}'>{$item['name']}</a>";
    echo '</div>';
}

//recursive function to output nested groups and items if exists
function printNestedGroupsAndItems($group, $groups, $items)
{
    echo '<div class="group">';
    echo "<h2>{$group['name']}({$group['id']})</h2>";
    $currentGroupId = $group['id'];
    foreach ($groups as $group) {
        if ($group['parent_group_id'] == $currentGroupId) {
            printNestedGroupsAndItems($group, $groups, $items);
        }
    }
    foreach ($items as $item) {
        if ($item['parent_group_id'] == $currentGroupId) {
            printItem($item);
        }
    }
    echo '</div>';
}
