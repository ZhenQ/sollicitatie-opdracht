<?php
require_once('config.php');

try {
    $groupsSql = "
        CREATE TABLE IF NOT EXISTS `groups` (
          `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
          `name` varchar(30) NOT NULL DEFAULT '',
          `parent_group_id` int(6) unsigned DEFAULT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;
        ";
    $dbh->exec($groupsSql);
    echo "-Groups table created<br/>";
} catch (PDOException $e) {
    echo $sql . "<br>" . $e->getMessage();
}

try {
    $groupsSql = "
        CREATE TABLE IF NOT EXISTS `items` (
          `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
          `name` varchar(30) NOT NULL DEFAULT '',
          `parent_group_id` int(6) unsigned DEFAULT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;
        ";
    $dbh->exec($groupsSql);
    echo "-Items table created<br/>";
} catch (PDOException $e) {
    echo $sql . "<br>" . $e->getMessage();
}

echo "<strong>Installation complete</strong>";