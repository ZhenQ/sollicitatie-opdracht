$(document).ready(function(){
    function init() {
        eventHandler();
    }
    function eventHandler(){
        var $form = $('form');
        var $loader = $('.loader');

        $form.on("submit", function (event) {
            event.preventDefault();

            $.ajax({
                method: "POST",
                url: "/action.php",
                dataType: "json",
                data: $(this).serialize(),
                beforeSend: function() {
                    $loader.show();
                }
            }).done(function() {
                location.reload();
            });
        });
    }
    init();

});

