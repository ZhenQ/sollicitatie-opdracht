<?php
require_once('config.php');

if (isset($_POST['group-name'])) {
    $stmt = $dbh->prepare("INSERT INTO groups (name, parent_group_id) VALUES (:name, :parent_group_id)");
    $name = $_POST['group-name'];
} elseif (isset($_POST['item-name'])) {
    $stmt = $dbh->prepare("INSERT INTO items (name, parent_group_id) VALUES (:name, :parent_group_id)");
    $name = $_POST['item-name'];
}
$name = filter_var($name, FILTER_SANITIZE_STRING);
$parentGroupId = filter_var(($_POST['parent-group-id']), FILTER_SANITIZE_NUMBER_INT);

$stmt->bindParam(':name', $name);
$stmt->bindParam(':parent_group_id', $parentGroupId);

echo $stmt->execute();